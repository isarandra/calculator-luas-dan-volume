// ISA RANDRA

import java.util.Scanner;

public class CalculatorLuasVolume {
    static Scanner scanner = new Scanner(System.in);
    static int controlMenuUtama, controlMenuLuas, controlMenuVolume;
    private static double sisi, luas, radius, alas, tinggi, panjang, lebar, volume;

    public static void main(String[] args) {
        header("Kalkulator Penghitung Luas dan Volume");
        menuUtama();
    }

    private static void menuLuas() {
        header("Pilih bidang yang akan dihitung luasnya");
        System.out.println("1. Persegi" +
                "\n2. Lingkaran" +
                "\n3. Segitiga" +
                "\n4. Persegi Panjang" +
                "\n0. Kembali ke Menu Sebelumnya");
        garis();
        controlMenuLuas = scanner.nextInt();
        switch (controlMenuLuas){
            case 1:
                luasPersegi();
                break;
            case 2:
                luasLingkaran();
                break;
            case 3:
                luasSegitiga();
                break;
            case 4:
                luasPersegiPanjang();
                break;
            case 0:
                menuUtama();
        }
    }

    private static void luasPersegiPanjang() {
        header("Anda memilih persegi panjang");
        System.out.print("Panjang: ");
        panjang = scanner.nextDouble();
        System.out.print("Lebar: ");
        lebar = scanner.nextDouble();
        processing();
        luas = panjang * lebar;
        System.out.println("Luas persegi panjang: " + luas);
    }

    private static void luasSegitiga() {
        header("Anda memilih segitiga");
        System.out.print("Alas: ");
        alas = scanner.nextDouble();
        System.out.print("Tinggi: ");
        tinggi = scanner.nextDouble();
        processing();
        luas = .5 * alas * tinggi;
        System.out.println("Luas segitiga: " + luas);
    }

    private static void luasLingkaran() {
        header("Anda memilih lingkaran");
        System.out.print("Panjang jari-jari: ");
        radius = scanner.nextDouble();
        processing();
        luas = Math.PI * Math.pow(radius, 2);
        System.out.println("Luas lingkaran: " + luas);
    }

    private static void luasPersegi() {
        header("Anda memilih persegi");
        System.out.print("Panjang sisi: ");
        sisi = scanner.nextDouble();
        luas = Math.pow(sisi, 2);
        processing();
        System.out.println("Luas persegi: " + luas);
    }

    private static void menuUtama() {
        System.out.println("Menu" +
                "\n1. Hitung Luas Bidang" +
                "\n2. Hitung Volume Bidang" +
                "\n0. Tutup Aplikasi");
        garis();
        controlMenuUtama = scanner.nextInt();
        switch (controlMenuUtama){
            case 1:
                menuLuas();
                break;
            case 2:
                menuVolume();
                break;
            case 0:
                System.exit(1);
        }
        pressEnterToContinue();
        menuUtama();
    }

    private static void menuVolume() {
        header("Pilih bidang yang akan dihitung volumenya");
        System.out.println("1. Kubus" +
                "\n2. Balok" +
                "\n3. Tabung" +
                "\n0. Kembali ke Menu Sebelumnya");
        garis();
        controlMenuVolume = scanner.nextInt();
        switch (controlMenuVolume){
            case 1:
                volumeKubus();
                break;
            case 2:
                volumeBalok();
                break;
            case 3:
                volumeTabung();
                break;
            case 0:
                menuUtama();
        }
    }

    private static void volumeTabung() {
        header("Anda memilih tabung");
        System.out.print("Jari-jari: ");
        radius = scanner.nextDouble();
        System.out.print("Tinggi: ");
        tinggi = scanner.nextDouble();
        processing();
        volume = Math.PI * Math.pow(radius, 2) * tinggi;
        System.out.println("Volume tabung: " + volume);
    }

    private static void volumeBalok() {
        header("Anda memilih balok");
        System.out.print("Panjang: ");
        panjang = scanner.nextDouble();
        System.out.print("Lebar: ");
        lebar = scanner.nextDouble();
        System.out.print("Tinggi: ");
        tinggi = scanner.nextDouble();
        processing();
        volume = panjang * lebar * tinggi;
        System.out.println("Volume balok: " + volume);
    }

    private static void volumeKubus() {
        header("Anda memilih kubus");
        System.out.print("Sisi: ");
        sisi = scanner.nextDouble();
        processing();
        volume = Math.pow(sisi, 3);
        System.out.println("Volume kubus: " + volume);
    }

    private static void header(String anyString) {
        garis();
        System.out.println(anyString);
        garis();
    }
    private static void processing() {
        System.out.println(" ");
        System.out.println("Processing...");
        System.out.println(" ");
    }
    private static void pressEnterToContinue(){
        garis();
        System.out.println("Tekan enter untuk kembali ke menu utama");
        try {
            System.in.read();
        } catch(Exception e){
        }
    }
    private static void garis() {
        System.out.println("---------------------------------------------");
    }
}
